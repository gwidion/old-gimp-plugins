WHAT IS THIS PROJECT
====================

This is a collection of GIMP-Python scripts I have been writting since
the dawn of times (GIMP pré 2.0, Python 2.2) and just kept floating
around my filesystems.

The scripts range from "hello world" types to fully fledged HTTP
servers interacting with GIMP. 

As of now, I just gathered a bit shame, and decide to properly 
publish them as a public acessible resource.  That does not mean
that any of them is usefull, documented, or even working at all.

Starting from now, I will go about fixing some of them, 
at least trying to write down what they do (or would be supposed to do),
and as possible, maintaining GIMP's development Python bindings as I go.


INSTALL
=======

All "*.py" files here are meant to be put in a GIMP's
plug-ins directory, marked as executable, and they
should show up somewhere in GIMP's menus upon restart.
They should work with GIMP 2.8 - as of this write,
GIMP 2.6, but no efforts will be made to keep 
this compatibility.

(You could even add this project's folder straight as a 
plug-in folder,  under edit->preferences->folders->plug-ins)

For Windows users: make shure you have gimp-python working.

