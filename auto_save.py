#!/usr/bin/env python
# -*- coding: utf-8 -*-

from gimpfu import *
import gtk
from gobject import timeout_add
import os, time

# (c) 2009 by Joao S. O. Bueno
# Licensed under the GNU Public License v.3.0 or later

class AutoSaveWindow(gtk.Window):
    def __init__ (self, interval):
        self.button = gtk.Button("Stop Auto Saving")
        self.interval = interval
        r =  gtk.Window.__init__(self)
        vbox = gtk.VBox()
        self.add(vbox)


        self.label = gtk.Entry()
        self.label.set_editable(False)
        self.clear_msg()
        vbox.pack_start(self.label)
        timeout_add(interval * 60 * 1000, self.update, self)
        vbox.pack_start(self.button)
        self.button.connect("clicked", self.quit)
        self.show_all()
        self.running = True

    def update(self, *args):
        if not self.running:
            return False
        names = set()
        for img in gimp.image_list():
            basename = name = img.name
            counter = 0
            while name in names:
                counter += 1
                name = "%s_%02d" % (basename, counter)
            names.add(name)
            if not name.endswith(".xcf"):
                name += ".xcf"
            #FIXME: do something to more consistent for images when name == "Untitled"
            new_image =  pdb.gimp_image_duplicate(img)
            file_name = os.path.join(SAVEDIR, name)
            hour = time.strftime("%H:%M")
            try:
                pdb.gimp_file_save(new_image, new_image.layers[0], file_name, file_name)
                self.label.set_text("%s: autosaved %s" %  (hour, file_name))
            except IndexError:
                # don't know how to save an image withoud layers.
                # the procedure call requires a drawable
                pass
            pdb.gimp_image_delete(new_image)
        timeout_add(15000, self.clear_msg, self)
        return True

    def clear_msg(self):
        self.label.set_text("Auto- saving enabled each %d minutes" % self.interval)
        return False

    def quit(self, *args):
        self.running = False
        gtk.main_quit()



def autosave(interval):
    global SAVEDIR
    SAVEDIR = pdb.gimp_gimprc_query("temp-path")
    r = AutoSaveWindow(interval)
    gtk.main()

register(
         "autosave",
         "Enables automathic saving of open images",
         """Saves an .xcf snapshot file all open images in GIMP's temporary folder.
Warning: this is a workaround for GIMP not implementing this feature
yet. Full discussion can be seen at https://bugzilla.gnome.org/show_bug.cgi?id=138373
This is simple as it is, and will mostly stay this way - there are no guarrantees about
the working or behavior of this script. It does not interact back with GIMP - just
issues a save command that may take place in background - operations being applied
are likely to be partially saved if at all. Yet, people seen to find some usefulness to
this script as it is.
         """,
         "Joao S. O. Bueno",
         "GPL v3",
         "2009",
         "Enable Auto-save...",
         "",
         [(PF_INT, "interval", "Interval for auto-save, in minutes", 5) ],
         [],
         autosave,
         menu="<Image>/File/")

main()
